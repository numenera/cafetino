package com.cafetino.cafetino;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cafetino.cafetino.dao.ContatoDAO;
import com.cafetino.cafetino.model.Contato;

public class FormContatoActivity extends AppCompatActivity {

    TextView txtID;
    EditText txtNome;
    EditText txtSaldo;
    ContatoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_contato);
        txtID = (TextView) findViewById(R.id.txtID);
        txtNome = (EditText) findViewById(R.id.txtNome);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);

        txtID.setText("0");
        txtSaldo.setText("0");

        txtNome.requestFocus();
    }

    public void salvar(View view) {
        dao = new ContatoDAO(getBaseContext());
        Contato item = new Contato(Integer.parseInt(txtID.getText().toString()),
                txtNome.getText().toString(),
                Integer.parseInt(txtSaldo.getText().toString()));
        if (!dao.atualizar(item)) {
            dao.inserir(item);
        }
        ;
    }
}
