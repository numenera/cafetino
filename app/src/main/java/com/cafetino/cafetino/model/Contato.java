package com.cafetino.cafetino.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luana on 25/12/2016.
 */

public class Contato implements Parcelable {
    private int id;
    private String nome;
    private int saldo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public Contato() {
    }

    public Contato(int id, String nome, int saldo) {
        this.id = id;
        this.nome = nome;
        this.saldo = saldo;
    }

    private Contato(Parcel from) {
        id = from.readInt();
        nome = from.readString();
        saldo = from.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nome);
        dest.writeInt(saldo);
    }

    public String toString() {
        return "_ID=" + id +
                ", Nome=" + nome +
                ", Saldo= " + saldo;
    }

    public static final Creator<Contato>
            CREATOR = new Creator<Contato>() {
        public Contato createFromParcel(Parcel in) {
            return new Contato(in);
        }

        public Contato[] newArray(int size) {
            return new Contato[size];
        }
    };
}
