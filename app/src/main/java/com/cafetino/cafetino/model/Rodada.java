package com.cafetino.cafetino.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luana on 25/12/2016.
 */

public class Rodada implements Parcelable {
    private int id;
    private String local;
    private String data;
    private Double valor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Rodada() {
    }

    public Rodada(int id, String local, String data, Double valor) {
        this.id = id;
        this.local = local;
        this.data = data;
        this.valor = valor;
    }

    private Rodada(Parcel from) {
        id = from.readInt();
        local = from.readString();
        data = from.readString();
        valor = from.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(local);
        dest.writeString(data);
        dest.writeDouble(valor);
    }

    public String toString() {
        return "Rodada=" + id +
                ", Local=" + local +
                ", Data=" + data +
                ", Valor=" + valor;
    }

    public static final Parcelable.Creator<Rodada>
            CREATOR = new Parcelable.Creator<Rodada>() {
        public Rodada createFromParcel(Parcel in) {
            return new Rodada(in);
        }

        public Rodada[] newArray(int size) {
            return new Rodada[size];
        }
    };
}
