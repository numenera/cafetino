package com.cafetino.cafetino.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luana on 25/12/2016.
 */

public class Movimento implements Parcelable {
    private int id_rodada;
    private int id_contato;
    private int quantidade;

    public int getId_rodada() {
        return id_rodada;
    }

    public void setId_rodada(int id_rodada) {
        this.id_rodada = id_rodada;
    }

    public int getId_contato() {
        return id_contato;
    }

    public void setId_contato(int id_contato) {
        this.id_contato = id_contato;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Movimento() {
    }

    public Movimento(int id_contato, int id_rodada, int quantidade) {
        this.id_contato = id_contato;
        this.id_rodada = id_rodada;
        this.quantidade = quantidade;
    }

    private Movimento(Parcel from) {
        id_contato = from.readInt();
        id_rodada = from.readInt();
        quantidade = from.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id_contato);
        dest.writeInt(id_rodada);
        dest.writeInt(quantidade);
    }

    public String toString() {
        return "Contato=" + id_contato +
                ", Rodada=" + id_rodada +
                ", Quantidade=" + quantidade;
    }

    public static final Parcelable.Creator<Movimento>
            CREATOR = new Parcelable.Creator<Movimento>() {
        public Movimento createFromParcel(Parcel in) {
            return new Movimento(in);
        }

        public Movimento[] newArray(int size) {
            return new Movimento[size];
        }
    };
}
