package com.cafetino.cafetino.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by luana on 25/12/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String BANCO_DADOS = "bd_cafetino";
    private static int VERSAO = 1;

    //inclusao de constantes - tabelas e colunas do BD
    public static class Contato {
        public static final String TABELA = "contato";
        public static final String _ID = "_id";
        public static final String NOME = "nome";
        public static final String SALDO = "saldo";

        public static final String[] COLUNAS = new String[]{_ID, NOME, SALDO};
    }

    public static class Rodada {
        public static final String TABELA = "rodada";
        public static final String _ID = "_id";
        public static final String LOCAL = "local";
        public static final String DATA = "data";
        public static final String VALOR = "valor";

        public static final String[] COLUNAS = new String[]{_ID, LOCAL, DATA, VALOR};
    }

    public static class Movimento {
        public static final String TABELA = "movimento";
        public static final String _ID_RODADA = "_id_rodada";
        public static final String _ID_CONTATO = "_id_contato";
        public static final String QUANTIDADE = "quantidade";

        public static final String[] COLUNAS = new String[]{_ID_RODADA, _ID_CONTATO, QUANTIDADE};
    }

    public DatabaseHelper(Context context) {
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //criacao das tabelas
        db.execSQL("Create table contato(_id   integer primary key autoincrement" +
                ",nome  text not null" +
                ",saldo integer);" +

                "Create table rodada(_id   integer primary key autoincrement" +
                ",local text" +
                ",data  text" +
                ",valor integer);" +

                "Create table movimento(_id_rodada  integer not null" +
                ",_id_contato integer not null" +
                ",quantidade  integer not null" +
                ",FOREIGN KEY (_id_rodada)  REFERENCES rodada(_id)" +
                ",FOREIGN KEY (_id_contato) REFERENCES contato(_id)" +
                ",PRIMARY KEY (_id_rodada, _id_contato));"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //atualizacao da estrutura do BD
    }

}
