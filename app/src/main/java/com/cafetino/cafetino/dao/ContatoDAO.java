package com.cafetino.cafetino.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cafetino.cafetino.model.Contato;

import java.util.ArrayList;

/**
 * Created by luana on 25/12/2016.
 */

public class ContatoDAO {
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    public ContatoDAO(Context context) {
        helper = new DatabaseHelper(context);
    }

    //obtem uma instancia de SQLiteDatabase, criando-a se necessario
    public SQLiteDatabase getDb() {
        if (db == null) {
            db = helper.getWritableDatabase();
        }
        return db;
    }

    public long inserir(Contato contato) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.Contato.NOME,
                contato.getNome());
        values.put(DatabaseHelper.Contato.SALDO,
                contato.getSaldo());
        return getDb().insert(DatabaseHelper.Contato.TABELA, null, values);

    }

    public ArrayList<Contato> listar() {
        Cursor cursor = getDb().query(DatabaseHelper.Contato.TABELA,
                DatabaseHelper.Contato.COLUNAS, null, null, null, null, null);
        ArrayList<Contato> contatos = new ArrayList<Contato>();
        while (cursor.moveToNext()) {
            Contato contato = new Contato();
            contato.setId(cursor.getInt(0));
            contato.setNome(cursor.getString(1));
            contato.setSaldo(cursor.getInt(2));
            contatos.add(contato);
        }
        cursor.close();
        return contatos;
    }

    public Contato buscaPorId(Integer id) {
        Cursor cursor = getDb().query(DatabaseHelper.Contato.TABELA,
                DatabaseHelper.Contato.COLUNAS,
                DatabaseHelper.Contato._ID + " = ?",
                new String[]{id.toString()},
                null, null, null);
        if (cursor.moveToNext()) {
            Contato contato = new Contato();
            contato.setId(cursor.getInt(0));
            contato.setNome(cursor.getString(1));
            contato.setSaldo(cursor.getInt(2));
            return contato;
        }
        cursor.close();
        return null;
    }

    public boolean atualizar(Contato contato) {
        int atualizados = 0;
        if (contato.getId()!=0) {
            String whereClause = DatabaseHelper.Contato._ID + " = ?";
            String[] whereArgs = new String[]{String.valueOf(contato.getId())};
            System.out.println(whereClause + whereArgs);

            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.Contato._ID,
                    contato.getId());
            values.put(DatabaseHelper.Contato.NOME,
                    contato.getNome());
            values.put(DatabaseHelper.Contato.SALDO,
                    contato.getSaldo());
            atualizados = getDb().update(DatabaseHelper.Contato.TABELA, values,
                    whereClause, whereArgs);
        }
        return atualizados > 0;
    }

    public boolean excluir(Long id) {
        String whereClause = DatabaseHelper.Contato._ID + " = ?";
        String[] whereArgs = new String[]{id.toString()};
        int removidos = getDb().delete(DatabaseHelper.Contato.TABELA,
                whereClause, whereArgs);
        return removidos > 0;
    }

    public void close() {
        helper.close();
    }
}
