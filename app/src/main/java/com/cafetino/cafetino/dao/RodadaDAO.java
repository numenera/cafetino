package com.cafetino.cafetino.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cafetino.cafetino.model.Rodada;

import java.util.ArrayList;

/**
 * Created by luana on 25/12/2016.
 */

public class RodadaDAO {
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    public RodadaDAO(Context context) {
        helper = new DatabaseHelper(context);
    }

    //obtem uma instancia de SQLiteDatabase, criando-a se necessario
    public SQLiteDatabase getDb() {
        if (db == null) {
            db = helper.getWritableDatabase();
        }
        return db;
    }

    public long inserir(Rodada rodada) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.Rodada._ID,
                rodada.getId());
        values.put(DatabaseHelper.Rodada.LOCAL,
                rodada.getLocal());
        values.put(DatabaseHelper.Rodada.DATA,
                rodada.getData());
        values.put(DatabaseHelper.Rodada.VALOR,
                rodada.getValor());
        return getDb().insert(DatabaseHelper.Rodada.TABELA, null, values);

    }

    public ArrayList<Rodada> listar() {
        Cursor cursor = getDb().query(DatabaseHelper.Rodada.TABELA,
                DatabaseHelper.Rodada.COLUNAS, null, null, null, null, null);
        ArrayList<Rodada> Rodadas = new ArrayList<Rodada>();
        while (cursor.moveToNext()) {
            Rodada Rodada = new Rodada();
            Rodada.setId(cursor.getInt(0));
            Rodada.setLocal(cursor.getString(1));
            Rodada.setData(cursor.getString(2));
            Rodada.setValor(cursor.getDouble(2));
            Rodadas.add(Rodada);
        }
        cursor.close();
        return Rodadas;
    }

    public Rodada buscaPorId(Integer id) {
        Cursor cursor = getDb().query(DatabaseHelper.Rodada.TABELA,
                DatabaseHelper.Rodada.COLUNAS,
                DatabaseHelper.Rodada._ID + " = ?",
                new String[]{id.toString()},
                null, null, null);
        if (cursor.moveToNext()) {
            Rodada Rodada = new Rodada();
            Rodada.setId(cursor.getInt(0));
            Rodada.setLocal(cursor.getString(1));
            Rodada.setData(cursor.getString(2));
            Rodada.setValor(cursor.getDouble(2));
            return Rodada;
        }
        cursor.close();
        return null;
    }

    public boolean atualizar(Rodada Rodada) {
        String whereClause = DatabaseHelper.Rodada._ID + " = ? AND ";
        String[] whereArgs = new String[]{String.valueOf(Rodada.getId())};
        System.out.println(whereClause + whereArgs);

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.Rodada._ID,
                Rodada.getId());
        values.put(DatabaseHelper.Rodada.LOCAL,
                Rodada.getLocal());
        values.put(DatabaseHelper.Rodada.DATA,
                Rodada.getData());
        values.put(DatabaseHelper.Rodada.VALOR,
                Rodada.getValor());
        int atualizados = getDb().update(DatabaseHelper.Rodada.TABELA, values,
                whereClause, whereArgs);
        return atualizados > 0;
    }

    public boolean excluir(Long id) {
        String whereClause = DatabaseHelper.Rodada._ID + " = ? ";
        String[] whereArgs = new String[]{id.toString()};
        int removidos = getDb().delete(DatabaseHelper.Rodada.TABELA,
                whereClause, whereArgs);
        return removidos > 0;
    }

    public void close() {
        helper.close();
    }
}
