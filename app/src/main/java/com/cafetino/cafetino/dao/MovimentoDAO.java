package com.cafetino.cafetino.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cafetino.cafetino.model.Movimento;

import java.util.ArrayList;

/**
 * Created by luana on 25/12/2016.
 */

public class MovimentoDAO {
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    public MovimentoDAO(Context context) {
        helper = new DatabaseHelper(context);
    }

    //obtem uma instancia de SQLiteDatabase, criando-a se necessario
    public SQLiteDatabase getDb() {
        if (db == null) {
            db = helper.getWritableDatabase();
        }
        return db;
    }

    public long inserir(Movimento movimento) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.Movimento._ID_CONTATO,
                movimento.getId_contato());
        values.put(DatabaseHelper.Movimento._ID_RODADA,
                movimento.getId_rodada());
        values.put(DatabaseHelper.Movimento.QUANTIDADE,
                movimento.getQuantidade());
        return getDb().insert(DatabaseHelper.Movimento.TABELA, null, values);

    }

    public ArrayList<Movimento> listar() {
        Cursor cursor = getDb().query(DatabaseHelper.Movimento.TABELA,
                DatabaseHelper.Movimento.COLUNAS, null, null, null, null, null);
        ArrayList<Movimento> movimentos = new ArrayList<Movimento>();
        while (cursor.moveToNext()) {
            Movimento Movimento = new Movimento();
            Movimento.setId_contato(cursor.getInt(0));
            Movimento.setId_rodada(cursor.getInt(1));
            Movimento.setQuantidade(cursor.getInt(2));
            movimentos.add(Movimento);
        }
        cursor.close();
        return movimentos;
    }

    public Movimento buscaPorIdContato(Integer id) {
        Cursor cursor = getDb().query(DatabaseHelper.Movimento.TABELA,
                DatabaseHelper.Movimento.COLUNAS,
                DatabaseHelper.Movimento._ID_CONTATO + " = ?",
                new String[]{id.toString()},
                null, null, null);
        if (cursor.moveToNext()) {
            Movimento Movimento = new Movimento();
            Movimento.setId_contato(cursor.getInt(0));
            Movimento.setId_rodada(cursor.getInt(1));
            Movimento.setQuantidade(cursor.getInt(2));
            return Movimento;
        }
        cursor.close();
        return null;
    }

    public Movimento buscaPorIdRodada(Integer id) {
        Cursor cursor = getDb().query(DatabaseHelper.Movimento.TABELA,
                DatabaseHelper.Movimento.COLUNAS,
                DatabaseHelper.Movimento._ID_RODADA + " = ?",
                new String[]{id.toString()},
                null, null, null);
        if (cursor.moveToNext()) {
            Movimento Movimento = new Movimento();
            Movimento.setId_contato(cursor.getInt(0));
            Movimento.setId_rodada(cursor.getInt(1));
            Movimento.setQuantidade(cursor.getInt(2));
            return Movimento;
        }
        cursor.close();
        return null;
    }

    public boolean atualizar(Movimento Movimento) {
        String whereClause = DatabaseHelper.Movimento._ID_CONTATO + " = ? AND " +
                DatabaseHelper.Movimento._ID_RODADA + " = ? ";
        String[] whereArgs = new String[]{String.valueOf(Movimento.getId_contato())
                , String.valueOf(Movimento.getId_rodada())};
        System.out.println(whereClause + whereArgs);

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.Movimento._ID_CONTATO,
                Movimento.getId_contato());
        values.put(DatabaseHelper.Movimento._ID_RODADA,
                Movimento.getId_rodada());
        values.put(DatabaseHelper.Movimento.QUANTIDADE,
                Movimento.getQuantidade());
        int atualizados = getDb().update(DatabaseHelper.Movimento.TABELA, values,
                whereClause, whereArgs);
        return atualizados > 0;
    }

    public boolean excluir(Long id_quantidade, Long id_rodada) {
        String whereClause = DatabaseHelper.Movimento._ID_CONTATO + " = ? AND " +
                DatabaseHelper.Movimento._ID_RODADA + " = ? ";
        String[] whereArgs = new String[]{id_quantidade.toString()
                , id_rodada.toString()};
        int removidos = getDb().delete(DatabaseHelper.Movimento.TABELA,
                whereClause, whereArgs);
        return removidos > 0;
    }

    public void close() {
        helper.close();
    }
}
